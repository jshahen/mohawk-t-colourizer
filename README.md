# Mohawk+T Colourizer

This extension adds colour to the ATRBAC-Safety policy files formatted in Mohawk+T format.

## Features

Currently the extension colours comments, keywords, time-intervals/time-slots, and role names.

![Screenshot](images/screenshot.png)

## Requirements

No requirements.

## Extension Settings

No settings available. Change colours in your theme editor.

## Known Issues

None
